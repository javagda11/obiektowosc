package enumPrzyklad;

public enum TypRoweru {
    BICYCLE(1), TANDEM(2);

    private int iloscMiejsc;

    TypRoweru(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }
}
