package enumPrzyklad;

public class Samochod {
    private String nazwa;
    private int predkosc;
    private TypSamochodu typ;

    public Samochod() {
    }

    public TypSamochodu getTyp() {
        return typ;
    }

    public void setTyp(TypSamochodu typ) {
        this.typ = typ;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }

    public void wypiszInformacjeOSamochodzie() {
        System.out.println("Informacje o samochodzie: \npredkosc: " + predkosc + " \nnazwa: " + nazwa);
    }

    public void przyspieszO5kmh() {
        predkosc += 5;
    }

    public void zwolnijO5kmh() {
        if (predkosc >= 5) {
            predkosc -= 5;
        } else {
            predkosc = 0;
        }
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nazwa='" + nazwa + '\'' +
                ", predkosc=" + predkosc +
                ", typ=" + typ +
                '}';
    }
}
