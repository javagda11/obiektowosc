package enumPrzyklad;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj nazwę: ");
        String liniaOdUzytkownika = sc.nextLine();

        Samochod samochod = new Samochod();
        samochod.setNazwa(liniaOdUzytkownika);

        System.out.println("Podaj typ samochodu (HATCHBACK/SEDAN/KOMBI): ");
        String liniaDruga = sc.nextLine();

        TypSamochodu typ = TypSamochodu.valueOf(liniaDruga.toUpperCase());
        samochod.setTyp(typ);

        System.out.println(samochod);


//        samochod.setTyp(TypSamochodu.HATCHBACK);
//        samochod.setNazwa("Mercedes");
//
////        System.out.println(samochod);
//
//        TypSamochodu[] dostepneWartoscsi = TypSamochodu.values();
//        for (TypSamochodu typ : dostepneWartoscsi) {
//            System.out.println(typ);
//        }
    }
}
