package cwiczenieKomputery;

public class Laptop extends Komputer{
    private double wielkoscMatrycy;
    private boolean czyMaRetine;

    public Laptop(int potrzebnaMoc, String producent,
                  TypProcesora typProcesora, double wielkoscMatrycy,
                  boolean czyMaRetine) {
        super(potrzebnaMoc, producent, typProcesora);

        this.wielkoscMatrycy = wielkoscMatrycy;
        this.czyMaRetine = czyMaRetine;
    }

    @Override
    public void wypiszInformacjeOKomputerze() {
        super.wypiszInformacjeOKomputerze();
        System.out.println(", wielkoscMatrycy=" + wielkoscMatrycy +
                ", czyMaRetine=" + czyMaRetine +
                '}');
    }
}
