package zad2;

public class Main {
    public static void main(String[] args) {
        Samochod samochod = new Samochod();
        Samochod samochod2 = new Samochod();
        samochod2.setNazwa("costam");
        samochod.wypiszInformacjeOSamochodzie();

        samochod.przyspieszO5kmh();
        samochod.wypiszInformacjeOSamochodzie();
        samochod2.wypiszInformacjeOSamochodzie();

        samochod.przyspieszO5kmh();
        samochod.wypiszInformacjeOSamochodzie();
        samochod2.wypiszInformacjeOSamochodzie();
    }
}
