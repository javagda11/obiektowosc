package zad2;

public class Samochod {
    private String nazwa;
    private int predkosc;

    public Samochod() {
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }

    public void wypiszInformacjeOSamochodzie() {
        System.out.println("Informacje o samochodzie: \npredkosc: " + predkosc + " \nnazwa: " + nazwa);
    }

    public void przyspieszO5kmh() {
        predkosc += 5;
    }

    public void zwolnijO5kmh() {
        if (predkosc >= 5) {
            predkosc -= 5;
        } else {
            predkosc = 0;
        }
    }
}
