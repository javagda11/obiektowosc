package zad1;

public class SalaLekcyjna {
    // enkapsulacja
    // hermetyzacja
    private String nazwa;
    private int numerSali;

    public SalaLekcyjna() {
    }

    public SalaLekcyjna(String nazwa, int numerSali) {
        this.nazwa = nazwa;
        this.numerSali = numerSali;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getNumerSali() {
        return numerSali;
    }

    public void setNumerSali(int numerSali) {
        this.numerSali = numerSali;
    }

    @Override
    public String toString() {
        return "Siema jestem sala lekcyjna i mam numer " + numerSali;
    }
}
