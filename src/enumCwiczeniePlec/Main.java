package enumCwiczeniePlec;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Imie:");
        String imie = scanner.nextLine();

        System.out.println("Nazwisko:");
        String nazwisko = scanner.nextLine();

        System.out.println("Plec:");
        String plec = scanner.nextLine();

        System.out.println("Pesel:");
        String pesel = scanner.nextLine();

        Plec enumPlec = Plec.valueOf(plec);
        Obywatel obywatel = new Obywatel(imie, nazwisko, pesel, enumPlec);


        System.out.println(obywatel);
//        System.out.println(obywatel.toString()); // to samo co wyzej

        System.out.println("Imie obywatela: ");
        System.out.println(obywatel.getImie());

    }
}
