package enumCwiczenie1;

public enum Bilet {
    ULGOWY_GODZINNY(1.7, 60),
    ULGOWY_CALODNIOWY(7.0, 1440),
    NORMALNY_GODZINNY(3.4, 60),
    NORMALNY_CALODNIOWY(14.0, 1440),
    BRAK_BILETU(0, 0);

    private double cena;
    private int czasJazdy;

    Bilet(double cena, int czasJazdy) {
        this.cena = cena;
        this.czasJazdy = czasJazdy;
    }

    public double pobierzCene(){
        return cena;
    }

    public int pobierzCzasPrzejazdu(){
        return czasJazdy;
    }

    public void wyswietlDaneOBilecie(){
        System.out.println("To jest bilet: " + this + " czas jazdy: " + czasJazdy);
    }
}
