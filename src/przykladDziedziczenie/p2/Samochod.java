package przykladDziedziczenie.p2;

public class Samochod {
    protected int predkosc;
    protected boolean swiatlaWlaczone;

    public void przyspiesz() {
        predkosc += 10;
    }

    public void setSwiatlaWlaczone(boolean swiatlaWlaczone) {
        this.swiatlaWlaczone = swiatlaWlaczone;
    }

    public boolean czySwiatlaWlaczone() {
        return this.swiatlaWlaczone;
    }
}
