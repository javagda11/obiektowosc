package przykladDziedziczenie.p1;

import przykladDziedziczenie.p2.Samochod;

public class Kabriolet extends Samochod {
    private boolean dachSchowany;

    public void schowajDach() {
        dachSchowany = true;
    }

    public boolean czyDachSchowany() {
        return dachSchowany;
    }

    public void wypiszOSobie() {
        System.out.println("Jestem sobie kabrio i mam " + dachSchowany);
        System.out.println("Moja predkosc to : " + this.predkosc);
        System.out.println("Swiatla : " + this.swiatlaWlaczone);
    }

    @Override
    public String toString() {
        return "Kabriolet{" +
                "dachSchowany=" + dachSchowany +
                '}';
    }
}
